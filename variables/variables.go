package variables

import (
	"io/ioutil"
	"log"
	"os"
	"encoding/json"
	"time"
	)

//----------------------------------------------------------------------------------------------------------------------
// Type definitions
//----------------------------------------------------------------------------------------------------------------------

type botConfigType struct {
	Token string `json:"Token"`
	AdminId     int   `json:"AdminId"`
	AdminChatId int64 `json:"AdminChatId"`
}

type botStringsType struct {
	Welcome string `json:"Welcome"`
	Closing string `json:"Closing"`
	NotAllowed string `json:"NotAllowed"`
	AddedToShout string `json:"AddedToShout"`
	RemovedFromShout string `json:"RemovedFromShout"`
	NotAllowedShout string `json:"NotAllowedShout"`
}

type strNamesType struct {
	Uniques [][2]string `json:"uniques"`
	Names []string `json:"names"`
	Surnames []string `json:"surnames"`
}


//----------------------------------------------------------------------------------------------------------------------
// Global variables
//----------------------------------------------------------------------------------------------------------------------

var BotConfig botConfigType
var BotStrings botStringsType

var activeChats map[int64]time.Time
var ChatUserlists map[int64] map[int]bool
var StrNames strNamesType
var ConfigRoute string

//----------------------------------------------------------------------------------------------------------------------
// Functions
//----------------------------------------------------------------------------------------------------------------------


func LoadBotConfig(configRoute string) int {
	file, error := ioutil.ReadFile(configRoute)

	if error != nil{
		log.Fatal("File error: %v\n", error)
		os.Exit(1)
	}
	error = json.Unmarshal(file, &BotConfig)

	log.Printf("Loaded Bot Config")
	return 0
}

func LoadBotStrings() int{
	file, error := ioutil.ReadFile("./data/strings.json")

	if error != nil{
		log.Fatal("File error: %v\n", error)
		os.Exit(1)
	}
	error = json.Unmarshal(file, &BotStrings)

	log.Printf("Loaded Bot Strings")
	return 0
}

func LoadChatUserlists() int {
	file, error := ioutil.ReadFile("./data/chatUserLists.json")

	if error != nil{
		log.Fatal("File error: %v\n", error)
		os.Exit(1)
	}
	error = json.Unmarshal(file, &ChatUserlists)

	log.Printf("Loaded Chat Userlists")
	return 0
}

func LoadSTRNames()int{
	file, error := ioutil.ReadFile("./data/strnames.json")

	if error != nil{
		log.Fatal("File error: %v\n", error)
		os.Exit(1)
	}
	error = json.Unmarshal(file, &StrNames)

	log.Printf("Loaded STR names")
	return 0
}

func SaveBotConfig() int {
	newConfig, _ := json.Marshal(BotConfig)
	file, _ := os.OpenFile(ConfigRoute, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0666)
	defer file.Close()
	file.Write(newConfig)

	return 0
}

func SaveChatUserlists() int {
	newUserLists, _ := json.Marshal(ChatUserlists)
	file, _ := os.OpenFile("./data/ChatUserLists.json", os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0666)
	defer file.Close()
	file.Write(newUserLists)

	return 0
}

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------