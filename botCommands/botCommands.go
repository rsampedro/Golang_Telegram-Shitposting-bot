package botCommands


//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

import (
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"strings"
	"Shitposting_Bot/variables"
	"strconv"
	"math/rand"
)

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

var CommandMap map[string](func(*tgbotapi.BotAPI, *tgbotapi.Message, *map[string]interface{}) int)

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

func CommandBestGirl(bot *tgbotapi.BotAPI,	message *tgbotapi.Message,
	parameters *map[string]interface{}) int{

	image := tgbotapi.NewPhotoUpload(message.Chat.ID, "./images/haru.png")
	image.ReplyToMessageID = message.MessageID
	image.Caption = "Haru is the best girl!!!"
	bot.Send(image)

	return 0
}

func CommandDiagonalize(bot *tgbotapi.BotAPI, message *tgbotapi.Message,
	parameters *map[string]interface{}) int{

	diagonalString := ""

	for _, word := range strings.Split(message.Text, " ")[1:]{
		wordSliced := strings.Split(word, "")
		wordSpaced := strings.Join(wordSliced, " ")
		wordJumped := ""
		for idx, letter := range wordSliced[1:]{
			newSlice := ""
			if wordSliced[0] == wordSliced[len(wordSliced)-1]{
				if idx == len(wordSliced)-2{
					newSlice = wordSpaced
				}else{
					newSlice = letter+strings.Repeat(" ", idx*2 +1)+letter+
						strings.Repeat(" ", (len(wordSliced)-idx)*2-5)+letter+"\n"
				}
			}else{
				newSlice = letter+strings.Repeat(" ", idx*2 +1)+letter+"\n"

			}
			wordJumped = wordJumped + newSlice
		}

		s := []string{diagonalString, wordSpaced, wordJumped}
		diagonalString = strings.Join(s, "\n")
	}
	diagonalString = "`" +diagonalString+"`"

	msg := tgbotapi.NewMessage(message.Chat.ID, diagonalString)
	msg.ParseMode = "Markdown"
	msg.ReplyToMessageID = message.MessageID

	bot.Send(msg)
	return 0
}

func CommandModifyShoutList (bot *tgbotapi.BotAPI, message *tgbotapi.Message,
	parameters *map[string]interface{}) int{
	fromMessage := message.From.ID
	currentChatUserlist := make(map[int]bool)
	if val, ok := variables.ChatUserlists[message.Chat.ID]; ok {
		currentChatUserlist = val
	}
	var response string
	if currentChatUserlist[fromMessage]{
		response = variables.BotStrings.RemovedFromShout + message.Chat.Title + "("+strconv.Itoa(int(message.Chat.ID))+")"
		delete(currentChatUserlist, fromMessage)
	}else{
		response = variables.BotStrings.AddedToShout + message.Chat.Title + "("+strconv.Itoa(int(message.Chat.ID))+")"
		currentChatUserlist[fromMessage] = true
	}
	variables.ChatUserlists[message.Chat.ID] = currentChatUserlist
	msg := tgbotapi.NewMessage(int64(message.From.ID), response)
	bot.Send(msg)
	return 0
}

func CommandShout (bot *tgbotapi.BotAPI, message *tgbotapi.Message,
	parameters *map[string]interface{}) int{
	currentChatID := message.Chat.ID
	messageBody := message.Text
	messageBody = strings.TrimPrefix(messageBody,"/shout")
	messageBody = strings.TrimPrefix(messageBody, bot.Self.UserName)
	messageBody = strings.TrimPrefix(messageBody, "@"+bot.Self.UserName)
	var userlist string
	var response string

	if _, exists := variables.ChatUserlists[currentChatID][message.From.ID]; exists {
		for user, _ := range variables.ChatUserlists[currentChatID]{
			userinchat := tgbotapi.ChatConfigWithUser{currentChatID, "" ,user}
			chatmember, _ := bot.GetChatMember(userinchat)
			if chatmember.Status == "member"{
				userlist = userlist + "@" + chatmember.User.UserName+" "
			}else{
				delete(variables.ChatUserlists[currentChatID], user)
			}
		}
		response = userlist + messageBody
		msg := tgbotapi.NewMessage(message.Chat.ID, response)
		bot.Send(msg)
	}else{
		msg := tgbotapi.NewMessage(message.Chat.ID, variables.BotStrings.NotAllowedShout)
		msg.ReplyToMessageID = message.MessageID
		bot.Send(msg)
	}

	return 0
}

func CommandSTRName (bot *tgbotapi.BotAPI, message *tgbotapi.Message,
	parameters *map[string]interface{}) int{
	response := ""

	uniqueChance := 100
	roll := rand.Intn(1000)
	uniques := variables.StrNames.Uniques
	nameSlice := variables.StrNames.Names
	surnameSlice := variables.StrNames.Surnames
	name:= ""
	surname := ""
	var rare bool
	// ROLL NAMES
	if roll < uniqueChance {
		rare = true
		roll = rand.Intn(len(uniques))
		name = uniques[roll][0]
		surname = uniques[roll][1]
	}else{
		rare = false
		roll = rand.Intn(len(nameSlice))
		name = nameSlice[roll]
		roll = rand.Intn(len(surnameSlice))
		surname = surnameSlice[roll]
	}

	if rare{
		response = "**UNIQUE**\n"
	}
	response = response + "Your STR Name is: "+name+" "+surname
	msg := tgbotapi.NewMessage(message.Chat.ID, response)
	msg.ReplyToMessageID = message.MessageID
	bot.Send(msg)

	return 0
}


func initializeCommandMap ()int{
	CommandMap = map[string](func(*tgbotapi.BotAPI, *tgbotapi.Message, *map[string]interface{}) int){
		"diagonalize": CommandDiagonalize,
		"bestgirl": CommandBestGirl,
		"modifyshoutlist" : CommandModifyShoutList,
		"shout" : CommandShout,
		"strname": CommandSTRName,
		}
	addAdminCommands()
	return 0
}

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

func init(){
	initializeCommandMap()
}

