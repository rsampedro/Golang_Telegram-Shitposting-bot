package main


import (
	"log"
	"time"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"os"
	"bufio"
	"strings"
	"strconv"
	"Shitposting_Bot/botCommands"
	"Shitposting_Bot/variables"
)

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

// Bot variables
var knownChats []int64

//----------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

func treatUpdates(bot *tgbotapi.BotAPI, updates tgbotapi.UpdatesChannel, err error){

	commandMap := botCommands.CommandMap


	for update := range updates {
		if update.Message == nil {
			//In case we've received no messages
			continue
		} else {
			log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
			// a := bot.IsMessageToMe(*update.Message)
			// log.Print(a)

			messageSlice := strings.Split(update.Message.Text, " ")
			command := messageSlice[0]

			if update.Message.IsCommand(){
				command = update.Message.Command()
				if _, command_exists := commandMap[command]; command_exists {
					parameters := map[string](interface{}){}
					_ = commandMap[command](bot, update.Message, &parameters)
				}
			}
		}
	}
}

func automaticTasks(){
	log.Print("Starting automatic tasks")
	for (true){
		time.Sleep(time.Second * 60)
		variables.SaveChatUserlists()
		variables.SaveBotConfig()
		log.Print("Saved Bot data")
	}

}

func initializeBot(bot *tgbotapi.BotAPI, err *error)(updates tgbotapi.UpdatesChannel){

	// Initialize Data and read if it already exists
	os.Mkdir("data", 666)

	knownChats = loadKnownChats()

	msg := tgbotapi.NewMessage(int64(variables.BotConfig.AdminChatId), variables.BotStrings.Welcome)
	bot.Send(msg)


	update := tgbotapi.NewUpdate(0)
	update.Timeout = 10


	updates, *err = bot.GetUpdatesChan(update)
	updates.Clear()

	return
}

func loadKnownChats() []int64 {
	file, error := os.Open("./data/chats.txt")
	if error != nil{
		file, error := os.OpenFile("./data/chats.txt", os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0666)
		defer file.Close()
		if error != nil {
			log.Fatal("File error: %v\n", error)
		}
	}

	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanWords)

	for scanner.Scan() {             // internally, it advances token based on sperator
		id := scanner.Text()
		id_int, error := strconv.ParseInt(id, 10, 64)
		if error != nil {
			log.Fatal("File error: %v\n", error)
			os.Exit(1)
		}
		knownChats = append(knownChats, id_int)
		log.Printf("Read id as "+id)
	}
	log.Printf("Loaded known chats")

	return knownChats
}

func main(){
	variables.ConfigRoute = "./config.json"

	variables.LoadBotConfig(variables.ConfigRoute)
	variables.LoadBotStrings()
	variables.LoadChatUserlists()
	variables.LoadSTRNames()



	bot, err := tgbotapi.NewBotAPI(variables.BotConfig.Token)
	if err != nil {
		log.Panic(err)
	}
	updateChann := initializeBot(bot, &err)

	bot.Debug = false

	log.Printf("Authorized on account %s", bot.Self.UserName)
	go treatUpdates(bot, updateChann, err)

	automaticTasks()
}

